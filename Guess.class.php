<?php

namespace App\Core;

/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
{
    /**
     * @var CardGame un jeu de cartes
     */
    private $cardGame;

    /**
     * @var Card c'est la carte à deviner par le joueur
     */
    private $selectedCard;

    /**
     * @var bool pour prendre en compte lors d'une partie
     */
    private $withHelp;
}
