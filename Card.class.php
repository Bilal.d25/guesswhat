<?php
namespace App\Core;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
    /**
     * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
     */
    private $name;

    /**
     * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
     */
    private $color;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    // [...]
}
